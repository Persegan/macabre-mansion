﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class popup_text_choices : MonoBehaviour {

    public string[] messages;
    public string[] choicemessages;
    public string[] messageschoice1;
    public string[] messageschoice2;
    public Image dialoguebox;
    public Text displaytext;
    public GameObject player;
    public AudioSource sound;
    public Button Choice1;
    public Button Choice2;

    public string variable1;
    public int value1;
    public string variable2;
    public int value2;

    private int state = -1;

    private int choice_level = 0;

    private bool caninteract = false;

    // Use this for initialization
    void Start()
    {
        state = -1;
        choice_level = 0;
        caninteract = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (choice_level == 1)
            {
                if (choice_button_script.choice == 0)
                {
                    if (state == messageschoice1.Length - 1)
                    {
                        player.GetComponent<Player_Movement>().can_move = true;
                        dialoguebox.gameObject.SetActive(false);
                        displaytext.gameObject.SetActive(false);
                        state = -1;
                        choice_level = 0;
                        choice_button_script.choice = 1;
                        if (variable1 != "" || value1 != 0)
                        {
                            player_data.playerdata.global_variables[variable1] = value1;
                        }
                    }
                    else if (caninteract == true)
                    {
                        GameObject myEventSystem = GameObject.Find("EventSystem");
                        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
                        Choice1.GetComponentInChildren<Text>().text = "";
                        Choice2.GetComponentInChildren<Text>().text = "";
                        Choice1.gameObject.SetActive(false);
                        Choice2.gameObject.SetActive(false);
                        state++;
                        sound.Play();
                        player.GetComponent<Player_Movement>().can_move = false;
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = messageschoice1[state];
                       
                    }
                }
                else if (choice_button_script.choice == 1)
                {
                    if (state == messageschoice2.Length - 1)
                    {
                        choice_level = 0;
                        player.GetComponent<Player_Movement>().can_move = true;
                        dialoguebox.gameObject.SetActive(false);
                        displaytext.gameObject.SetActive(false);
                        state = -1;
                        choice_button_script.choice = 1;
                        if (variable2 != "" || value2 != 0)
                        {
                            player_data.playerdata.global_variables[variable2] = value2;
                        }

                    }
                    else if (caninteract == true)
                    {
                        GameObject myEventSystem = GameObject.Find("EventSystem");
                        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
                        Choice1.GetComponentInChildren<Text>().text = "";
                        Choice2.GetComponentInChildren<Text>().text = "";
                        Choice1.gameObject.SetActive(false);
                        Choice2.gameObject.SetActive(false);
                        state++;
                        sound.Play();
                        player.GetComponent<Player_Movement>().can_move = false;
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = messageschoice2[state];
                    }

                }
            }
            else if (choice_level == 0)
            {
                if (state == messages.Length - 1)
                {
                    displaytext.text = choicemessages[0];
                    Choice1.gameObject.SetActive(true);
                    Choice2.gameObject.SetActive(true);
                    Choice1.GetComponentInChildren<Text>().text = choicemessages[1];
                    Choice2.GetComponentInChildren<Text>().text = choicemessages[2];
                    Choice1.Select();
                    choice_level = 1;
                    state = -1;
                }
                else if (caninteract == true && Player_Movement.pause == false && player.GetComponent<Player_Movement>().can_move == true)
                {
                    state++;
                    player.GetComponent<Player_Movement>().can_move = false;
                    dialoguebox.gameObject.SetActive(true);
                    displaytext.gameObject.SetActive(true);
                    displaytext.text = messages[state];
                    sound.Play();
                }
            }


        }
    }
    void OnTriggerStay2D(Collider2D collision)
    {
        if (Player_Movement.pause == false)
        {
            if (collision.tag == "Player_trigger")
            {
                if (collision.gameObject.GetComponentInParent<Player_Movement>().can_move == true)
                {
                    caninteract = true;
                }

            }
        }
        else
        {
            caninteract = false;
        }
    }

    void OnTriggerExit2D()
    {
        caninteract = false;
    }
}
