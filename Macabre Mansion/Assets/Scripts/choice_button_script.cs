﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class choice_button_script : MonoBehaviour {

    public AudioSource menu_choose;
    public AudioSource menu_highlight;

    public static int choice = 1;

	// Use this for initialization
	void Start () {
        choice = 0;
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void PlayMenuChoose()
    {
        menu_choose.Play();
    }
    
    public void PlayMenuHighlight()
    {
        menu_highlight.Play();
    }

    public void choiceincrease()
    {
        choice++;
    }
    public void choicedecrease()
    {
        choice--;
    }
}
