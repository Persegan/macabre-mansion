﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Variable_Object{
    public GameObject target;
    public string[] variables;
    public int[] values;
    public int requirednumber;
    public int currentumber = 0;
}
