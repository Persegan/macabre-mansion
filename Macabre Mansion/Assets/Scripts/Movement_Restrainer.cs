﻿using UnityEngine;
using System.Collections;

public class Movement_Restrainer : MonoBehaviour {

    public bool checknorth;
    public bool checksouth;
    public bool checkwest;
    public bool checkeast;

    public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag == "Impassable_Object")
        {
            if (checknorth == true)
            {
                player.GetComponent<Player_Movement>().can_move_north = false;
            }

            else if (checksouth == true)
            {
                player.GetComponent<Player_Movement>().can_move_south = false;
            }
            else if (checkeast == true)
            {
                player.GetComponent<Player_Movement>().can_move_east = false;
            }
            else if (checkwest == true)
            {
                player.GetComponent<Player_Movement>().can_move_west = false;
            }
        }
       
    }


    void OnTriggerExit2D(Collider2D collider)
    {

        if (collider.tag == "Impassable_Object")
        {
            if (checknorth == true)
            {
                player.GetComponent<Player_Movement>().can_move_north = true;
            }

            else if (checksouth == true)
            {
                player.GetComponent<Player_Movement>().can_move_south = true;
            }
            else if (checkeast == true)
            {
                player.GetComponent<Player_Movement>().can_move_east = true;
            }
            else if (checkwest == true)
            {
                player.GetComponent<Player_Movement>().can_move_west = true;
            }
        }
       
    }
}
