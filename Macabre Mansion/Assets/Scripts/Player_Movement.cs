﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player_Movement : MonoBehaviour {
    

    Vector3 pos;                                // For movement

    public float speed = 2.0f;                         // Speed of movement

    public float distance = 3.0f;

    public BoxCollider2D trigger_zone;

    public bool can_interact;

    public static bool pause;

    public bool can_move_north;
    public bool can_move_south;
    public bool can_move_east;
    public bool can_move_west;

    public GameObject PauseMenu;

    public AudioSource slash_sound;

    public bool can_move;

    private bool auxiliar_movement;

    // Use this for initialization
    void Start () {
        auxiliar_movement = false;
        pause = false;
        pos = transform.position;          // Take the initial position
        can_move_south = true;
        can_move_east = true;
        can_move_west = true;
        can_move_north = true;
        can_move = true;
        can_interact = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (auxiliar_movement == true)
        {
            if (transform.position == pos)
            {
                can_move = true;
                gameObject.GetComponent<Animator>().SetBool("Moving", false);
                auxiliar_movement = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (can_move == true && pause == false)
            {
                OpenMenu();
            }
            else if (can_move == true && pause == true)
            {
                CloseMenu();
            }
        }

            if (transform.position == pos && can_move == true)
        {
            gameObject.GetComponent<Animator>().SetBool("Moving", false);

            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                gameObject.GetComponent<Animator>().SetBool("Moving", true);
                gameObject.GetComponent<Animator>().SetInteger("Direction", 1);
                trigger_zone.offset = new Vector2(0, 0.5f);
                if (can_move_north == true)
                {
                    pos += Vector3.up * distance;
                    auxiliar_movement = true;
                    can_move = false;
                    StopAllCoroutines();

                }
               
            }

            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                gameObject.GetComponent<Animator>().SetBool("Moving", true);
                gameObject.GetComponent<Animator>().SetInteger("Direction", 3);
                trigger_zone.offset = new Vector2(0, -0.5f);
                if (can_move_south == true)
                {
                    pos += Vector3.down * distance;
                    auxiliar_movement = true;
                    can_move = false;
                    StopAllCoroutines();

                }
               
            }

            else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                gameObject.GetComponent<Animator>().SetBool("Moving", true);
                gameObject.GetComponent<Animator>().SetInteger("Direction", 4);
                trigger_zone.offset = new Vector2(-0.5f, 0);
                if (can_move_west == true)
                {
                    pos += Vector3.left * distance;
                    auxiliar_movement = true;
                    can_move = false;
                    StopAllCoroutines();

                }
   
            }

            else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                gameObject.GetComponent<Animator>().SetBool("Moving", true);
                gameObject.GetComponent<Animator>().SetInteger("Direction", 2);
                trigger_zone.offset = new Vector2(0.5f, 0);
                if (can_move_east == true)
                {
                    pos += Vector3.right * distance;
                    auxiliar_movement = true;
                    can_move = false;
                    StopAllCoroutines();

                }    
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                if (player_data.playerdata.global_variables["GotScalpel"] == 1 && gameObject.GetComponent<Animator>().GetBool("Moving") == false && can_move == true)
                {
                    StartCoroutine(Slash());
                }

            }
        }

        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);    // Move there
    }


    public void SlashAnimation()
    {
        StartCoroutine(SlashAnimationCoroutine());
    }

    public IEnumerator SlashAnimationCoroutine()
    {
        slash_sound.Play();
        GetComponent<Animator>().SetBool("Slashing", true);
        yield return new WaitForSeconds(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        GetComponent<Animator>().SetBool("Slashing", false);
    }

    IEnumerator Slash()
    {
        can_move = false;
        slash_sound.Play();
        GetComponent<Animator>().SetBool("Slashing", true);
        //yield return new WaitForSeconds(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length / (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed));
        yield return new WaitForSeconds(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        GetComponent<Animator>().SetBool("Slashing", false);
        can_move = true;
    }


    public void OpenMenu()
    {
        //Time.timeScale = 0;

        can_move = false;
        Player_Movement.pause = true;
        PauseMenu.SetActive(true);
        PauseMenu.transform.GetChild(1).gameObject.GetComponent<Button>().Select();
    }

    public void CloseMenu()
    {
        StartCoroutine(CloseMenuCoroutine());
      
        //Time.timeScale = 1;
    }

    public IEnumerator CloseMenuCoroutine()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        yield return new WaitForEndOfFrame();
        Player_Movement.pause = false;
        PauseMenu.SetActive(false);
        can_move = true;
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
