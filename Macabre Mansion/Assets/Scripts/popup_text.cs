﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class popup_text : MonoBehaviour {


	public string[] messages;
	public Image dialoguebox;
	public Text displaytext;
	public GameObject player;
	public AudioSource sound;
    public string variable;
    public int value;

    public int state = -1;

    public GameObject[] Store_Active_Gameobjects;

    protected bool caninteract = false;

    public bool x_interaction;

	// Use this for initialization
	void Start () {
        state = -1;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Z))
        {

            if (state == messages.Length - 1)
            {
                player.GetComponent<Player_Movement>().can_move = true;
                dialoguebox.gameObject.SetActive(false);
                displaytext.gameObject.SetActive(false);

                if (variable != "" || value != 0)
                {
                    player_data.playerdata.global_variables[variable] = value;
                }
                state = -1;
                if (x_interaction == true)
                {
                    state = -2;
                }
            }
            else if (caninteract == true && Player_Movement.pause == false && player.GetComponent<Player_Movement>().can_move == true && state > -2) {
                sound.Play();
                state++;
				player.GetComponent<Player_Movement> ().can_move = false;
                dialoguebox.gameObject.SetActive (true);
				displaytext.gameObject.SetActive (true);
                displaytext.text = messages[state];
			}
        }
        
        if (Input.GetKeyDown(KeyCode.X))
        {
              if (caninteract == true && Player_Movement.pause == false && player.GetComponent<Player_Movement>().can_move == true && state == -2)
            {
                {
                    sound.Play();
                    state = 0;
                    player.GetComponent<Player_Movement>().can_move = false;
                    dialoguebox.gameObject.SetActive(true);
                    displaytext.gameObject.SetActive(true);
                    displaytext.text = messages[state];
                }
            }
        }	
	}

	void OnTriggerStay2D(Collider2D collision)
	{
        if (Player_Movement.pause == false)
        {
            if (collision.tag == "Player_trigger")
            {
                if (collision.gameObject.GetComponentInParent<Player_Movement>().can_move == true)
                {
                    caninteract = true;
                }

            }
        }
        else
        {
            caninteract = false;
        }
		
	}

	void OnTriggerExit2D()
	{
        caninteract = false;
    }
}
