﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class Charley_popup_text : MonoBehaviour {
    public string[] messages;
    public Image dialoguebox;
    public Sprite dialoguebox2;
    public Text displaytext;
    public bool[] use_another_box;
    public GameObject player;
    public AudioSource sound;
    public string variable;
    public int value;

    public List<GameObject> Store_Active_Gameobjects;

    public int state = -1;

    protected bool caninteract = false;
    protected Sprite originalimage;

    public bool x_interaction;
    private bool x_pressed;

    // Use this for initialization
    void Start()
    {
        originalimage = dialoguebox.sprite;
        //state = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {

            if (state == messages.Length - 1)
            {
                player.GetComponent<Player_Movement>().can_move = true;
                dialoguebox.gameObject.SetActive(false);
                displaytext.gameObject.SetActive(false);
                dialoguebox.sprite = originalimage;

                player.GetComponent<Player_Movement>().can_interact = true;

                if (variable != "" || value != 0)
                {
                    player_data.playerdata.global_variables[variable] = value;
                }
                state = -1;

                for (int i = 0; i < Store_Active_Gameobjects.Count; i++)
                {
                    Store_Active_Gameobjects[i].SetActive(true);
                }
                Store_Active_Gameobjects.Clear();
                transform.parent.gameObject.GetComponent<Check_Global_Variables>().enabled = true;

                if (x_interaction == true)
                {
                    state = -2;
                    x_pressed = false;
                }

            }

            else if (caninteract == true && Player_Movement.pause == false)
            {
                if (x_interaction == true)
                {
                    if (x_pressed == true)
                    {
                        state++;
                        player.GetComponent<Player_Movement>().can_move = false;
                        player.GetComponent<Player_Movement>().can_interact = false;
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = messages[state];
                        sound.Play();
                        if (use_another_box[state] == true)
                        {
                            dialoguebox.sprite = dialoguebox2;
                        }
                        else
                        {
                            dialoguebox.sprite = originalimage;
                        }

                        if (state == 0)
                        {
                            Debug.Log("potato");
                            transform.parent.gameObject.GetComponent<Check_Global_Variables>().enabled = false;
                            {
                                for (int i = 0; i < transform.parent.childCount; i++)
                                {
                                    if (transform.parent.GetChild(i) == gameObject.transform)
                                    {

                                    }
                                    else
                                    {
                                        if (transform.parent.GetChild(i).gameObject.activeSelf == true)
                                        {
                                            Store_Active_Gameobjects.Add(transform.parent.GetChild(i).gameObject);
                                            transform.parent.GetChild(i).gameObject.SetActive(false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    state++;
                    player.GetComponent<Player_Movement>().can_move = false;
                    dialoguebox.gameObject.SetActive(true);
                    displaytext.gameObject.SetActive(true);
                    displaytext.text = messages[state];
                    sound.Play();
                    if (use_another_box[state] == true)
                    {
                        dialoguebox.sprite = dialoguebox2;
                    }
                    else
                    {
                        dialoguebox.sprite = originalimage;
                    }

                    if (state == 0)
                    {
                        transform.parent.gameObject.GetComponent<Check_Global_Variables>().enabled = false;
                        {
                            for (int i = 0; i < transform.parent.childCount; i++)
                            {
                                if (transform.parent.GetChild(i) == gameObject.transform)
                                {

                                }
                                else
                                {
                                    if (transform.parent.GetChild(i).gameObject.activeSelf == true)
                                    {
                                        Store_Active_Gameobjects.Add(transform.parent.GetChild(i).gameObject);
                                        transform.parent.GetChild(i).gameObject.SetActive(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }            
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            if (caninteract == true && Player_Movement.pause == false && player.GetComponent<Player_Movement>().can_interact == true && state == -2 && player_data.playerdata.global_variables["GotScalpel"] == 1)
            {
                player.GetComponent<Player_Movement>().can_interact = false;
                player.GetComponent<Player_Movement>().can_move = false;
                player.GetComponent<Player_Movement>().StopAllCoroutines();
                x_pressed = true;
                transform.parent.gameObject.GetComponent<Check_Global_Variables>().enabled = false;
                {
                    for (int i = 0; i < transform.parent.childCount; i++)
                    {
                        if (transform.parent.GetChild(i) == gameObject.transform)
                        {

                        }
                        else
                        {
                            if (transform.parent.GetChild(i).gameObject.activeSelf == true)
                            {
                                Store_Active_Gameobjects.Add(transform.parent.GetChild(i).gameObject);
                                transform.parent.GetChild(i).gameObject.SetActive(false);
                            }
                        }
                    }


                    sound.Play();
                    state = 0;
                    player.GetComponent<Player_Movement>().can_move = false;
                    player.GetComponent<Player_Movement>().SlashAnimation();
                    dialoguebox.gameObject.SetActive(true);
                    displaytext.gameObject.SetActive(true);
                    displaytext.text = messages[state];

                    if (use_another_box[state] == true)
                    {
                        dialoguebox.sprite = dialoguebox2;
                    }
                    else
                    {
                        dialoguebox.sprite = originalimage;
                    }
                }
            }
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (Player_Movement.pause == false)
        {
            if (collision.tag == "Player_trigger")
            {
                if (collision.gameObject.GetComponentInParent<Player_Movement>().can_move == true)
                {
                    caninteract = true;
                }

            }
        }
        else
        {
            caninteract = false;
        }
    }

    void OnTriggerExit2D()
    {
        caninteract = false;
    }
}
