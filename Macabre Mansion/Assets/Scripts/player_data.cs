﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class player_data : MonoBehaviour {

    public static player_data playerdata;

    public Dictionary<string, int> global_variables = new Dictionary<string, int>();

    void Awake()
    {
        if (playerdata == null)
        {
            DontDestroyOnLoad(gameObject);
            playerdata = this;
            playerdata.global_variables.Add("BonePile1", 0); // this saves the number of times you've interacted with the BonePile1
            playerdata.global_variables.Add("CharleyInteractions", 0); //This saves the amount of times you've talked to Charley
            playerdata.global_variables.Add("GotScalpel", 0); //This saves wether you have the scalpel or not

        }
        else
        {
            Destroy(gameObject);
        }
    }

	// Use this for initialization
	void Start () {

        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
